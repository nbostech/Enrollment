package com.wavelabs.dao;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.helper.Helper;
import com.wavelabs.model.Address;
import com.wavelabs.model.AutherizationTokens;
import com.wavelabs.model.User;
import com.wavelabs.service.GoogleGeoCode;

@Component
public class UserDao {
	@Autowired
	GoogleGeoCode googleGeoCode;

	public UserDao() {

	}

	public void saveUser(User user) {
		Session session = Helper.getSession();
		session.save(user);
		session.beginTransaction().commit();
		session.close();
	}

	public void socialUser(User user, AutherizationTokens token) {
		Session session = Helper.getSession();
		session.save(user);
		session.save(token);
		session.beginTransaction().commit();
		session.close();
	}

	public void updateuser(User user) {
		Session session = Helper.getSession();
		session.update(user);
		session.beginTransaction().commit();
		session.close();
	}

	public Address persistAddress(Address address, User user) {
		Session session = Helper.getSession();
		String[] result = googleGeoCode.getLL(address.getZipCode());
		address.setUser(user);
		address.setLatitude(result[0]);
		address.setLongitude(result[1]);
		session.save(address);
		session.beginTransaction().commit();
		return address;

	}
}
