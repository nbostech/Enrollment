package com.wavelabs.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.helper.Helper;
import com.wavelabs.model.Address;
import com.wavelabs.model.User;
import com.wavelabs.service.GoogleGeoCode;

@Component
public class AddressDao {
	@Autowired
	GoogleGeoCode googleGeoCode;

	public Address persistAddress(Address address, User user) {
		Session session = Helper.getSession();
		String[] result = googleGeoCode.getLL(address.getZipCode());
		address.setUser(user);
		address.setLatitude(result[0]);
		address.setLongitude(result[1]);
		session.save(address);
		Transaction transaction = session.beginTransaction();
		transaction.commit();
		return address;

	}
}
