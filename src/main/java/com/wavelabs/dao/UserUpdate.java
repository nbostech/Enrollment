package com.wavelabs.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.wavelabs.helper.Helper;
import com.wavelabs.model.User;

@Component
public class UserUpdate {
	public void updateDetails(User user) {
		Session session = Helper.getSession();
		session.saveOrUpdate(user);
		session.beginTransaction().commit();
		session.close();

	}

}
