package com.wavelabs.dao;

import org.springframework.stereotype.Component;

@Component
public class Message {
	private int statusCode;
	private String replyMessage;

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getReplyMessage() {
		return replyMessage;
	}

	public void setReplyMessage(String replyMessage) {
		this.replyMessage = replyMessage;
	}
}
