package com.wavelabs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.model.LoginType;
import com.wavelabs.model.Status;
import com.wavelabs.model.User;
import com.wavelabs.service.LoginValidation;
import com.wavelabs.service.SocialUserDetails;
import com.wavelabs.service.UpdatingDetails;
import com.wavelabs.service.UserConfiguration;

@RestController
@Component
public class Controller {
	@Autowired
	LoginValidation loginValidation;
	@Autowired
	UserConfiguration uc;
	UpdatingDetails ud = new UpdatingDetails();

	/**
	 * For a normal user who registers using the registration page.
	 * 
	 * @param user
	 * @return
	 */

	@RequestMapping(value = "/usercreate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String normaluser(@RequestBody User user) {
		user.setLoginType(LoginType.NORMALUSER);
		return uc.normaluser(user);

	}

	/**
	 * Here we are saving the facebook user.
	 * 
	 * @param user
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/fb/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)

	public String addfbUser(@RequestBody User user, @PathVariable("id") String id) {
		user.setLoginType(LoginType.FACEBOOK);
		user.setStatus(Status.ONE);
		return uc.addUser(user, id);
	}

	/**
	 * Here we are saving the google user.
	 * 
	 * @param user
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/gmail/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)

	public String addGmailUser(@RequestBody User user, @PathVariable("id") String id) {
		user.setLoginType(LoginType.GOOGLE);
		user.setStatus(Status.ONE);
		return uc.addUser(user, id);
	}

	/**
	 * Here we are validating the user while login. The email and the password
	 * are validated.
	 * 
	 * @param email
	 * @param password
	 * @return
	 */
	@RequestMapping(value = "/userlogin", method = RequestMethod.POST)

	public ResponseEntity<User> credentials(@RequestParam("email") String email,
			@RequestParam("password") String password) {
		User user = loginValidation.userValidation(email, password);
		if (user != null) {
			return new ResponseEntity<>(user, HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NON_AUTHORITATIVE_INFORMATION);
	}

	@RequestMapping(value = "/userupdate", method = RequestMethod.PUT)

	public String updateUser(User user, @PathVariable("id") int id) {
		return ud.updatingDetails(user);

	}

	@RequestMapping(value = "/social", method = RequestMethod.PUT)

	public ResponseEntity<User> credentials(@RequestParam("email") String email) {
		User user = SocialUserDetails.userValidation(email);
		if (user != null) {
			return new ResponseEntity<>(user, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);

		}
	}
}
