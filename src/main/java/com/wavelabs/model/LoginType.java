package com.wavelabs.model;

public enum LoginType {

	NORMALUSER, GOOGLE, FACEBOOK;
}
