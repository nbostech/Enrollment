package com.wavelabs.service;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.dao.AddressDao;
import com.wavelabs.dao.UserDao;
import com.wavelabs.helper.Helper;
import com.wavelabs.model.Address;
import com.wavelabs.model.AutherizationTokens;
import com.wavelabs.model.Status;
import com.wavelabs.model.User;

@Component
public class UserConfiguration {
	@Autowired
	UserDao userdao;

	public String addUser(User user, String tokenId) {

		Session session = Helper.getSession();
		String hql = "from " + User.class.getName() + " where email=:email";
		Query query = session.createQuery(hql);
		query.setParameter("email", user.getEmail());
		User userFromDb = (User) query.uniqueResult();
		if (userFromDb == null) {
			AutherizationTokens token = new AutherizationTokens();
			token.setUser(user);
			token.setTokenId(tokenId);
			userdao.socialUser(user, token);
		}
		return "welcome";
	}

	/**
	 * Here we are persisting the user registration, who is registering through
	 * normal process.
	 * 
	 * @param user
	 * @return
	 */
	public String normaluser(User user) {
		boolean flag = true;
		if (user.getStatus() == null) {
			user.setStatus(Status.ONE);
			userdao.saveUser(user);
			flag = false;
		} else if (user.getStatus() == Status.ONE) {
			user.setStatus(Status.TWO);
		} else if (user.getStatus() == Status.TWO) {
			user.setStatus(Status.THREE);
		} else if (user.getStatus() == Status.THREE) {
			user.setStatus(Status.FOUR);
		} else if (user.getStatus() == Status.FOUR) {
			AddressDao ad = new AddressDao();
			Address address = ad.persistAddress(user.getAddress(), user);
			address.setUser(user);
			user.setStatus(Status.COMPLETED);
		} else if (user.getStatus() == Status.COMPLETED) {
			return "welcome";
		}
		if (flag) {
			userdao.updateuser(user);
		}
		return "registeredsucessfully";
	}

}
