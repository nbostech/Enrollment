package com.wavelabs.service;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.wavelabs.helper.Helper;
import com.wavelabs.model.User;

@Component
public class SocialUserDetails {
	private SocialUserDetails() {

	}

	public static User userValidation(String email) {
		Logger log = Logger.getLogger(SocialUserDetails.class);
		User user = null;
		Session session = Helper.getSession();
		try {
			Criteria criteria = session.createCriteria(User.class);
			criteria.add(Restrictions.eq("email", email));
			user = (User) criteria.uniqueResult();
			user.getAddress().setUser(null);
		} catch (Exception e) {
			log.info(e);
		} finally {

			session.close();
		}
		return user;
	}
}
