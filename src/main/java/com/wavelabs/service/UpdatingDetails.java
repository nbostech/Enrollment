package com.wavelabs.service;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.wavelabs.dao.UserUpdate;
import com.wavelabs.model.Status;
import com.wavelabs.model.User;

@Component
public class UpdatingDetails {
	public String updatingDetails(@RequestBody User user) {
		UserUpdate userupdate = new UserUpdate();
		boolean flag = true;

		if (user.getStatus() == null) {
			user.setStatus(Status.ONE);
			userupdate.updateDetails(user);
			flag = false;
		}

		if (user.getStatus() == Status.ONE) {
			user.setExperience(user.getExperience());
			user.setHighestQualification(user.getHighestQualification());
			user.setGender(user.getGender());
			user.setStatus(Status.TWO);
			userupdate.updateDetails(user);
		} else if (user.getStatus() == Status.TWO) {
			user.setStatus(Status.THREE);
		} else if (user.getStatus() == Status.THREE) {
			user.setStatus(Status.FOUR);
		} else if (user.getStatus() == Status.FOUR) {
			user.setStatus(Status.COMPLETED);
		} else if (user.getStatus() == Status.COMPLETED) {
			return "welcome";
		}
		if (flag) {
			userupdate.updateDetails(user);
		}
		return "registeredsucessfully";
	}
}
