package com.wavelabs.model.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wavelabs.model.Gender;
import com.wavelabs.model.LoginType;
import com.wavelabs.model.Status;
import com.wavelabs.model.User;

public class UserTest {

	private User user;

	@Before
	public void user() {
		user = new User();
		user.setId(1);
		user.setFirstName("rajashekhar");
		user.setLastName("reddy");
		user.setEmail("shekhar@gmail.com");
		user.setPassword("1234aadd");
		user.setExperience(2);
		user.setHighestQualification("Btech");
		user.setMobile("8500000000");
		user.setGender(Gender.MALE);
		user.setIntrestedAreas("politics");
		user.setOthers("politics");
		user.setAddress(null);
		user.setFatherName("rangareddy");
		user.setMotherName("aruna");
		user.setLoginType(LoginType.NORMALUSER);
		user.setStatus(Status.COMPLETED);

	}

	@Test
	public  void testUser() {

		Assert.assertEquals(1, user.getId());
		Assert.assertEquals("rajashekhar", user.getFirstName());
		Assert.assertEquals("reddy", user.getLastName());
		Assert.assertEquals("shekhar@gmail.com", user.getEmail());
		Assert.assertEquals("1234aadd", user.getPassword());
		Assert.assertEquals(new Integer(2), user.getExperience());
		Assert.assertEquals("Btech", user.getHighestQualification());
		Assert.assertEquals("8500000000", user.getMobile());
		Assert.assertEquals(Gender.MALE, user.getGender());
		Assert.assertEquals("politics", user.getIntrestedAreas());
		Assert.assertEquals("politics", user.getOthers());
		Assert.assertEquals(null, user.getAddress());
		Assert.assertEquals("rangareddy", user.getFatherName());
		Assert.assertEquals("aruna", user.getMotherName());
		Assert.assertEquals(LoginType.NORMALUSER, user.getLoginType());
		Assert.assertEquals(Status.COMPLETED, user.getStatus());

	}

	@Test
	public void testSetId(){
	
		User user = new User();
		user.setId(11);
		Assert.assertEquals(11,user.getId());
	}
}
