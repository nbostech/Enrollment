package com.wavelabs.model.test;

import org.junit.Assert;
import org.junit.Test;

import com.wavelabs.model.AutherizationTokens;

public class AutherizationTokensTest {

	@Test
	public  void testAuthorizationTokens() {
		AutherizationTokens authorizationtokens = new AutherizationTokens();
		authorizationtokens.setId(111);
		authorizationtokens.setTokenId("214578569354");
		authorizationtokens.setUser(null);
		Assert.assertEquals(111, authorizationtokens.getId());
		Assert.assertEquals("214578569354", authorizationtokens.getTokenId());
		Assert.assertEquals(null, authorizationtokens.getUser());
	}

}
