package com.wavelabs.model.test;

import org.junit.Before;
import org.junit.Test;
import com.wavelabs.model.Address;
import com.wavelabs.model.Gender;
import com.wavelabs.model.User;

import org.junit.Assert;

public class AddressTest {
	private Address address;
	private User user;

	@Before
	public void addressInput() {

		address = new Address();
		address.setId(22);
		address.setCity("karimnagar");
		address.setStreet("ramnagar");
		address.setZipCode("500001");
		address.setState("telangana");
		address.setLatitude("17.0225514");
		address.setLongitude("22.01427584");
	}

	@Before
	public void user() {
		user = new User();
		user.setId(1);
		user.setFirstName("rajashekhar");
		user.setLastName("reddy");
		user.setEmail("shekhar@gmail.com");
		user.setPassword("1234aadd");
		user.setExperience(2);
		user.setHighestQualification("Btech");
		user.setMobile("8500000000");
		user.setGender(Gender.MALE);
		user.setIntrestedAreas("politics");
		user.setOthers("politics");
		user.setFatherName("rangareddy");
		user.setMotherName("aruna");

	}

	@Test
	public void testAddress() {
		Assert.assertEquals(22, address.getId());
		Assert.assertEquals("karimnagar", address.getCity());
		Assert.assertEquals("ramnagar", address.getStreet());
		Assert.assertEquals("telangana", address.getState());
		Assert.assertEquals("500001", address.getZipCode());
		Assert.assertEquals("17.0225514", address.getLatitude());
		Assert.assertEquals("22.01427584", address.getLongitude());
		Assert.assertEquals(1, user.getId());
		Assert.assertEquals("rajashekhar", user.getFirstName());
		Assert.assertEquals("reddy", user.getLastName());
		Assert.assertEquals("shekhar@gmail.com", user.getEmail());
		Assert.assertEquals("1234aadd", user.getPassword());
		Assert.assertEquals(new Integer(2), user.getExperience());
		Assert.assertEquals("Btech", user.getHighestQualification());
		Assert.assertEquals("8500000000", user.getMobile());
		Assert.assertEquals(Gender.MALE, user.getGender());
		Assert.assertEquals("politics", user.getIntrestedAreas());
		Assert.assertEquals("politics", user.getOthers());
		Assert.assertEquals("rangareddy", user.getFatherName());
		Assert.assertEquals("aruna", user.getMotherName());
	}

}
