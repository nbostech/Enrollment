package com.wavelabs.service.test;

import com.wavelabs.model.Address;
import com.wavelabs.model.Gender;
import com.wavelabs.model.LoginType;
import com.wavelabs.model.Status;
import com.wavelabs.model.User;

public class DataBuilder {

	public static Address address() {
		Address address = new Address();
		address.setId(22);
		address.setCity("karimnagar");
		address.setStreet("ramnagar");
		address.setZipCode("500001");
		address.setState("telangana");
		address.setLatitude("17.0225514");
		address.setLongitude("22.01427584");
		address.setUser(user());

		return address;

	}

	public static User user() {
		User user = new User();
		user.setId(1);
		user.setFirstName("rajashekhar");
		user.setLastName("reddy");
		user.setEmail("shekhar@gmail.com");
		user.setPassword("1234aadd");
		user.setExperience(2);
		user.setHighestQualification("Btech");
		user.setMobile("8500000000");
		user.setGender(Gender.MALE);
		user.setIntrestedAreas("politics");
		user.setOthers("politics");
		user.setAddress(null);
		user.setFatherName("rangareddy");
		user.setMotherName("aruna");
		user.setLoginType(LoginType.NORMALUSER);
		user.setStatus(Status.COMPLETED);
		return user;
	}
}
