/*package com.wavelabs.dao.test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.wavelabs.helper.Helper;
import com.wavelabs.model.Address;
import com.wavelabs.model.User;
import com.wavelabs.service.test.DataBuilder;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Helper.class)
public class AddressDaoTest {
	private Session session;
	 
	 @Mock
	 Transaction transaction;

	 @Before
	 public void setUp() {
	  session = mock(Session.class);
	  PowerMockito.mockStatic(Helper.class);
	  when(Helper.getSession()).thenReturn(session);
	 }

	@Test
	public void testAddressPersist(){
		Address address = DataBuilder.address();
		User user = DataBuilder.user();
		when(session.save(any(Address.class))).thenReturn(123);
		  when(session.beginTransaction()).thenReturn(transaction);
		  Address address1 = ObjectBuilder.getAddressDao().persistAddress(address, user);
		  Assert.assertEquals(address, address1);
		
		
	}
	
}
*/