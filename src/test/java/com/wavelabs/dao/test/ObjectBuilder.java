package com.wavelabs.dao.test;

import com.wavelabs.dao.AddressDao;
import com.wavelabs.dao.UserDao;
import com.wavelabs.dao.UserUpdate;

public class ObjectBuilder {
	 private static AddressDao addressDao;
	 private static UserDao userDao;
	 private static UserUpdate userUpdate;

	 public static AddressDao getAddressDao() {
	  if (addressDao == null) {
		  addressDao = new AddressDao();
	  }
	  return addressDao;
	 }
	 public static UserDao getuserDao() {
		  if (userDao == null) {
			  userDao = new UserDao();
		  }
		  return userDao;
		 }
	}
