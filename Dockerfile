FROM java:8
ADD target/EnrollmentProject.jar EnrollmentProject.jar
ENTRYPOINT ["java","-jar","EnrollmentProject.jar"]
